/**
 * Created by Dmitry Vereykin on 7/24/2015.
 */

public class MonthDemo {
   public static void main(String[] args) throws InvalidNameOfMonth, InvalidNumberOfMonth {

      try {
         Month m = new Month(32);
      } catch (InvalidNumberOfMonth invalidNumberOfMonth) {
         System.out.println(invalidNumberOfMonth.getMessage());
      }

      try {
         Month March = new Month("Morch");
      } catch (InvalidNameOfMonth invalidNameOfMonth) {
         System.out.println(invalidNameOfMonth.getMessage());
      }

   }
}
   
