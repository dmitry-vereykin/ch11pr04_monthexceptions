/**
 * Created by Dmitry Vereykin on 7/24/2015.
 */
public class InvalidNumberOfMonth extends Exception {
    public InvalidNumberOfMonth(int number) {
        super("Error! Month number " + number + " doesn't exist on this planet.");
    }
}
